import React from 'react';
import Navigation from './src/navigation';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {Persistor, Store} from './src/redux/store';
import messaging from '@react-native-firebase/messaging';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import {StatusBar} from 'react-native';
import {unique_id} from './src/assets';

messaging().onMessage(async remoteMessage => {
  setReduxData(remoteMessage);
});

// background
messaging().setBackgroundMessageHandler(async remoteMessage => {
  setReduxData(remoteMessage);
});

const setReduxData = async Message => {
  try {
    const data = {
      type: 'INBOX',
      payload: {
        title: Message.notification.title,
        body: Message.notification.body,
        data: Message.data,
        image: Message.notification.android.imageUrl,
        timestamp: new Date(),
        isRead: 0,
        id: unique_id(),
      },
    };
    Store.dispatch(data);
    // Store.dispatch();
  } catch (e) {
    console.log(e);
  }
};

const setInboxData = async remoteMessage => {
  try {
    let getInboxData = await AsyncStorageLib.getItem('newInboxData');
    getInboxData = JSON.parse(getInboxData);

    if (!getInboxData) {
      AsyncStorageLib.setItem(
        'newInboxData',
        JSON.stringify([
          {
            title: remoteMessage.notification.title,
            body: remoteMessage.notification.body,
            data: remoteMessage.data,
            image: remoteMessage.notification.android.imageUrl,
            timestamp: new Date(),
            isRead: 0,
            id: unique_id(),
          },
        ]),
      );
    } else {
      const arrayData = [
        ...[
          {
            title: remoteMessage.notification.title,
            body: remoteMessage.notification.body,
            data: remoteMessage.data,
            image: remoteMessage.notification.android.imageUrl,
            timestamp: new Date(),
            isRead: 0,
            id: unique_id(),
          },
        ],
        ...getInboxData,
      ].slice(0, 20);
      AsyncStorageLib.setItem('newInboxData', JSON.stringify(arrayData));
    }
  } catch (error) {
    console.log(error);
  }
};

const App = () => {
  return (
    <>
      <StatusBar backgroundColor="#f2f2f2" barStyle="dark-content" />
      <Provider store={Store}>
        <PersistGate loading={null} persistor={Persistor}>
          <Navigation />
        </PersistGate>
      </Provider>
    </>
  );
};

export default App;
