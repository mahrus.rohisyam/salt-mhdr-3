import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import MI from 'react-native-vector-icons/MaterialIcons';

export default class CTextInput extends Component {
  render() {
    const {firstIcon, secondIcon, secure} = this.props;

    return (
      <View style={styles.wrapper}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <MCI name={firstIcon} size={30} />
          <TextInput style={firstIcon? {width:'85%'}: {width:'90%'} } {...this.props} />
        </View>
        {secure && (
          <TouchableOpacity {...this.props}>
            <MCI name={secondIcon} size={30} />
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    padding: 2,
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
});
