import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';

export default class ScreenCard extends Component {
  render() {
    const {title, color} = this.props
    return (
      <TouchableOpacity style={[styles.btn, {backgroundColor: color}]} {...this.props}>
        <Text style={{color: 'white'}}>{title}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  btn:{
    borderRadius:15,
    padding: 20,
    backgroundColor:'red',
    width: 350,
    height: 150
  }
});
