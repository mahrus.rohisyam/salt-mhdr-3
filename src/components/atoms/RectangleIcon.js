import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';
import MI from 'react-native-vector-icons/MaterialIcons';

export default class RectangleIcon extends Component {
  render() {
    const {icon} = this.props;

    return (
      <TouchableOpacity style={styles.btn} {...this.props}>
        <MI name={icon} size={35} />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  btn: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    borderWidth: 1,
  },
});
