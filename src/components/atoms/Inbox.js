import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';
import {colors, Placeholder} from '../../assets';

export default class Inbox extends Component {
  render() {
    const {body, title, data, isRead, image, timestamps, style} = this.props;

    return (
      <TouchableOpacity
        {...this.props}
        activeOpacity={0.7}
        style={[
          styles.main,
          styles.unreadContainer,
          style
        ]}>
        <View>
          <Image
            style={styles.image}
            source={image ? {uri: image} : Placeholder}
          />
        </View>
        <View>
          <Text style={styles.unreadText}>
            {title}
          </Text>
          <Text style={{marginBottom: 20}}>{body}</Text>
          <Text style={{fontSize: 12}}>{timestamps}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    padding: 15,
    flexDirection: 'row',
  },
  unreadContainer: {
    backgroundColor: colors.primary,
    borderRadius: 20,
  },
  unreadText: {
    fontWeight: 'bold',
  },
  image: {
    width: 75,
    height: 75,
    borderRadius: 10,
    marginRight: 10,
  },
  readContainer: {
    borderRadius: 20,
    borderWidth: 0.5
  },
  readText: {
    fontWeight: 'normal',
  },
});
