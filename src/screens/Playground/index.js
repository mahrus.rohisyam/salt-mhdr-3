import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  convertDate,
  convertDateTime,
  convertTime,
  convertTimestamp,
} from '../../assets';

export class Index extends Component {
  constructor() {
    super();
    this.state = {
      inbox: [],
      buttonList: [
        {name: 'Fibonnacci', action: 'None'},
        {name: 'Fibonnacci', action: 'None'},
        {name: 'Fibonnacci', action: 'None'},
        {name: 'Fibonnacci', action: 'None'},
        {name: 'Fibonnacci', action: 'None'},
      ],
      array: [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        20,
      ],
    };
  }

  getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('inbox');
      console.log(jsonValue);
      if (jsonValue != null) {
        // const data = JSON.parse(jsonValue);
        // console.log(data);
        // const dataSort = data.sort((a, b) => {
        //   // Turn your strings into dates, and then subtract them
        //   // to get a value that is either negative, positive, or zero.
        //   return (
        //     convertTimestamp(new Date(JSON.parse(b.sentTime || b.from))) -
        //     convertTimestamp(new Date(JSON.parse(a.sentTime || a.from)))
        //   );
        // });
        // this.setState({
        //   inbox: dataSort,
        // });
      }
    } catch (e) {
      console.log(e);
    }
  };

  componentDidMount() {
    // this.getData();
    // console.log('Masuk');
    this._getInboxRedux();
  }

  _getInboxRedux = async () => {
    const {inbox} = this.state
    try {
      const {inboxData} = this.props;
      this.setState({inbox: [...inboxData]})
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const {inbox, array} = this.state;
    const {test} = this.props;
    console.log(inbox)

    return (
      <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
        {/* {array.map((value, i) => {  
          return <View>{value % 2 != 0 && <Text>{value}</Text>}</View>;
        })} */}
        {inbox.map((v, i)=> {
          console.log(v)
        })}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    test: state.test,
    inboxData: state.inbox,
  };
};

export default connect(mapStateToProps)(Index);
