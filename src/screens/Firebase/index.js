import {Text, StyleSheet, View, Image} from 'react-native';
import React, {Component} from 'react';
import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class Index extends Component {
  constructor() {
    super();
    this.state = {
      data: {},
    };
  }

  getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('inbox');
      if (jsonValue != null) {
        const data = JSON.parse(jsonValue);
        console.log(data);

        this.setState({
          inbox: data,
        });
      }
    } catch (e) {
      console.log(e);
    }
  };

  __getData = () => {
    AsyncStorage.getItem('inbox')
      .then(resultInbox => {
        const inbox = JSON.parse(resultInbox);
        console.log(resultInbox);
        if (resultInbox) {
          this.setState({
            inbox: inbox,
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  componentDidMount() {
    // firestore()
    //   .collection('Users')
    //   .doc('6Tlm53EKDNWvbzRJwodw')
    //   .onSnapshot(res => {
    //     this.setState({data: res.data()});
    //   });
    // firestore()
    //   .collection('Users')
    //   .onSnapshot(value => {
    //     let tampungan = value.docs.map(result => {
    //       return result.data();
    //     });
    //     tampungan.length > 0 && this.setState({data: tampungan});
    //   });
    this.getData();
  }

  render() {
    const {data} = this.state;
    console.log(data);

    return (
      <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
        <Text>Testing Firebase Data</Text>
        {/* <Image
          source={{
            uri: 'https://media-exp1.licdn.com/dms/image/C5603AQEbaikJhaGNCA/profile-displayphoto-shrink_800_800/0/1639446208466?e=1649894400&v=beta&t=UgEs7VWHCh4hmj9SLD-E1sM_W4J7rY1jiqs_OgbngZo',
          }}
          style={{height: 500, width: 500}}
        /> */}
        {/* {data.length != null &&
          data.data.map(v => {
            return <Text>{v.name}</Text>;
          })}
        <Text>{data[1].name}</Text> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
