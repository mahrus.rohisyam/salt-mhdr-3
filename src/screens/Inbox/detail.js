import {Text, StyleSheet, View, ScrollView, Image} from 'react-native';
import React, {Component} from 'react';
import {CText, Gap, RectangleIcon} from '../../components';
import {convertDate, convertDateTime, convertTime} from '../../assets';

export default class Detail extends Component {
  render() {
    const data = this.props.route.params;
    const {navigation} = this.props;
    console.log(data);

    return (
      <View style={{margin: 25}}>
        <View style={styles.header}>
          <RectangleIcon
            icon={'arrow-back'}
            onPress={() => {
              navigation.goBack();
            }}
          />
          <Gap width={20} />
          <CText style={styles.title}>Notification</CText>
        </View>
        <ScrollView>
          {Object.entries(data).forEach(([key, value]) => {
            return <Text>Test</Text>;
            console.log(value);
            // console.log(value)
          })}

          {Object.keys(data).forEach((key, index) => {
            console.log(data[key]);
          })}

          {/* <Text>{data.title}</Text>
          <Image style={styles.image} source={{uri: data.image}} />
          <Text>{data.body}</Text>
          <Text>{convertDateTime(new Date(data.timestamp))}</Text> */}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: 300,
    margin: 10,
  },
});
