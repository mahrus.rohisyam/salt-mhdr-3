import {Text, StyleSheet, View, ScrollView} from 'react-native';
import React, {Component} from 'react';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import {CText, Gap, Inbox, RectangleIcon} from '../../components';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import {colors, convertDateTime} from '../../assets';
import { connect } from 'react-redux';

export class Index extends Component {
  constructor() {
    super();
    this.state = {
      inbox: [],
    };
  }

  async componentDidMount() {
    const {inbox, students} = this.props
    console.log(inbox, 'Dari inbox')
    // console.log(students, 'Dari Students') 
    // this._getData();
  }

  // _getData = async () => {
  //   try {
  //     const jsonValue = await AsyncStorageLib.getItem('newInboxData');
  //     return jsonValue != null && this.setState({inbox: JSON.parse(jsonValue)});
  //   } catch (e) {
  //     console.log(e);
  //   }
  // };

  _deleteNotif = async () => {
    try {
      this.setState({inbox: []});
      await AsyncStorageLib.removeItem('newInboxData');
    } catch (e) {
      console.log(e);
    }
  };

  _readData = v => {
    const {navigation} = this.props;
    const {inbox} = this.state;
    navigation.navigate('Detail', v);
    const readData = inbox.map(value => {
      if (value.id === v.id) {
        value.isRead = 1;
        return value;
      } else {
        return value;
      }
    });
    console.log(inbox);
  };

  render() {
    // const {inbox} = this.state;
    const {navigation, inbox} = this.props;

    return (
      <View style={{margin: 25}}>
        <View style={styles.header}>
          <RectangleIcon
            icon={'arrow-back'}
            onPress={() => {
              navigation.goBack();
            }}
          />
          <Gap width={20} />
          <CText style={styles.title}>Notification</CText>
        </View>
        <Gap height={50} />
        <View style={styles.wrapper}>
          <CText style={styles.markRead}>Mark all as A read</CText>
          <MCI onPress={this._deleteNotif} name="trash-can" size={20} />
        </View>
        <ScrollView>
          {/* {inbox.length > 0 ? (
            inbox.map((v, i) => {
              return (
                <View key={i} style={{marginVertical: 5}}>
                  <Inbox
                    style={v.isRead == 1 && styles.readContainer}
                    onPress={() => this._readData(v)}
                    title={v.title}
                    data={v.data}
                    body={v.body}
                    image={v.image}
                    timestamps={convertDateTime(new Date(v.timestamp))}
                  />
                </View>
              );
            })
          ) : (
            <View style={{alignItems: 'center', marginVertical: 20}}>
              <Text style={{fontSize: 20, color: colors.primary}}>
                Yah kasian gada yang kasih notif :p
              </Text>
            </View>
          )} */}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    inbox: state.inbox,
    students: state.students
  }
}

// const mapDispatchToProps = state => {
//   return {}
// }

export default connect(mapStateToProps)(Index)

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
  },
  markRead: {
    textAlign: 'right',
    marginBottom: 5,
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 10,
  },
  readContainer: {
    borderRadius: 20,
    borderWidth: 0.5
  },
});
