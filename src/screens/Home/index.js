import React, {Component} from 'react';
import {Text, StyleSheet, View, Button, TextInput} from 'react-native';
import {CButton, CText, Layout, ScreenCard} from '../../components';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import auth from '@react-native-firebase/auth';
import {connect} from 'react-redux';
import {randomHexColor} from '../../assets';
import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';

export class Index extends Component {
  constructor() {
    super();
    this.state = {
      menuList: [
        {name: 'Lifecycle', route: 'Lifecycle'},
        {name: 'Props Communication', route: 'Propscom'},
        {name: 'Film API', route: 'Film'},
        {name: 'Statement', route: 'Statement'},
        {name: 'Login', route: 'Login'},
        {name: 'CRUD Statement', route: 'CRUD_Statement'},
        {name: 'Axios', route: 'EAxios'},
        {name: 'Playground', route: 'Playground'},
        {name: 'Redux', route: 'Redux'},
        {name: 'Fun Game', route: 'Fungame'},
        {name: 'Firebase', route: 'Firebase'},
        {name: 'Asyncstorage', route: 'Asyncscreen'},
        {name: 'Inbox', route: 'Inbox'},
      ],
    };
  }

  componentDidMount() {
    const {inboxData, students} = this.props;
    // console.log(inboxData)
    this._saveInbox();
    // messaging()
    //   .getToken()
    //   .then(res => {
    //     console.log(res);
    //   });
  }

  _saveInbox = async () => {
    const {_addInbox} = this.props;

    try {
      let inbox = await AsyncStorage.getItem('newInboxData');
      inbox != null && JSON.parse(inbox);
      _addInbox(inbox);
      // console.log(inbox)
    } catch (e) {
      console.log(e);
    }
  };

  _signOut = () => {
    const {deleteUser, currentUser, navigation} = this.props;
    auth()
      .signOut()
      .then(() => {
        deleteUser(null);
        console.log(currentUser);
        navigation.replace('Splash');
      });
  };

  render() {
    const {data, menuList, test} = this.state;
    const {navigation, currentUser} = this.props;

    return (
      <Layout scroll style={{flex: 1}}>
        <Text>Welcome To Class !</Text>
        <Text>Mau belajar apa harini ?</Text>
        {/* {menuList.map((v, i) => {
          return (
            <View style={{marginVertical: 5}} color key={i}>
              <Button
                color={i % 2 == 0 ? '#ffae00' : 'blue'}
                title={v.name}
                onPress={() => {
                  navigation.navigate(v.route);
                }}
              />
            </View>
          );
        })} */}
        {/* <Button
          title="Signout"
          color="red"
          onPress={() => {
            this._signOut();
          }}
        /> */}
        {menuList.map((v, i) => {
          return (
            <View style={{marginVertical: 5, alignItems: 'center'}} key={i}>
              <ScreenCard
                color={randomHexColor()}
                title={v.name}
                onPress={() => {
                  navigation.navigate(v.route);
                }}
              />
            </View>
          );
        })}
      </Layout>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    deleteUser: data => {
      dispatch({
        type: 'LOGIN',
        payload: data,
      });
    },
    _addInbox: data => {
      dispatch({
        type: 'ADD-INBOX',
        payload: data,
      });
    },
  };
};

const mapStateToProps = state => {
  return {
    currentUser: state.currentUser,
    inboxData: state.inbox,
    students: state.students,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);

const styles = StyleSheet.create({
  card: {
    margin: 10,
    padding: 5,
    borderWidth: 0.5,
    borderRadius: 10,
    elevation: 5,
    backgroundColor: 'white',
  },
});
