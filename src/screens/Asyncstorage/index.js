import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  Button,
  TouchableOpacity,
} from 'react-native';
import {CButton, Layout} from '../../components';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons';
import {colors, unique_id} from '../../assets';

export default class Index extends Component {
  constructor() {
    super();
    this.state = {
      data: [
        {id: 1, name: 'Mahrus', address: 'Tangerang'},
        {id: 2, name: 'Farid', address: 'Lampung'},
        {id: 3, name: 'Subhan', address: 'Jakarta'},
        {id: 4, name: 'Pur', address: 'Purwokerto'},
        {id: 5, name: 'Joni', address: 'Tangerang'},
        {id: 6, name: 'Dimitri', address: 'Rusia'},
      ],
      name: '',
      address: '',
      onEdit: true,
    };
  }

  async componentDidMount() {
    // this._saveData();
    // this._getData();
    //   console.log('Lets go')
  }

  _saveData = async () => {
    const stringValue = 'Test Al kocheng';
    const objectValue = {
      name: 'Mahrus',
      address: 'Bekasi',
    };
    const arrayValue = [1, 2, 3, 4];

    const convertToString = JSON.stringify(arrayValue);

    try {
      await AsyncStorage.setItem('TestStorage', convertToString);
    } catch (e) {
      console.log(e);
    }
  };

  _getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('TestStorage');
      return (
        jsonValue != null && this.setState({data: JSON.parse(jsonValue)})
      );
    } catch (e) {
      // error reading value
    }
  };

  render() {
    const {data, name, address, onEdit} = this.state;
    const white = 'white';

    return (
      <Layout scroll>
        <Text style={{textAlign: 'center', marginVertical: 10}}>
          Crud in State
        </Text>
        <View style={styles.card}>
          <View style={styles.tableHeader}>
            <Text style={styles.number}>ID</Text>
            <Text style={styles.title}>Name</Text>
            <Text style={styles.title}>Address</Text>
            <Text style={[styles.title, {width: '20%', textAlign: 'center'}]}>
              Action
            </Text>
          </View>
          {data.map((v, i) => {
            return (
              <View key={i} style={styles.tableHeader}>
                <Text style={styles.number}>{i + 1}</Text>
                <Text style={styles.title}>{v.name}</Text>
                <Text style={styles.title}>{v.address}</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '20%',
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity>
                    <MCI
                      name={
                        v.checked ? 'checkbox-blank' : 'checkbox-blank-outline'
                      }
                      onPress={() => this._onSelect(v.id)}
                      size={30}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={this._onEdit}>
                    <MCI
                      name={v.checked ? 'file-edit' : 'file-edit-outline'}
                      size={30}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            );
          })}
        </View>

        <View style={styles.textInputWrapper}>
          <TextInput
            placeholderTextColor={white}
            value={name}
            onChangeText={type => {
              this.setState({name: type});
            }}
            style={{width: '100%', color: colors.primary}}
            placeholder="Name"
          />
          <TextInput
            placeholderTextColor={white}
            value={address}
            onChangeText={type => {
              this.setState({address: type});
            }}
            style={{width: '100%', color: colors.primary}}
            placeholder="Address"
          />
          {name.length > 0 && address.length > 0 ? (
            <CButton title="ADD DATA" onPress={this._addData} />
          ) : (
            <View style={styles.fakeButton}>
              <Text>INSERT DATA</Text>
            </View>
          )}
        </View>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    padding: 10,
    borderRadius: 20,
    borderWidth: 1,
    margin: 5,
  },
  textInput: {
    justifyContent: 'space-around',
    marginTop: 15,
    flexDirection: 'row',
  },
  tableHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  spacer: {
    marginHorizontal: 10,
  },
  number: {
    width: '10%',
    alignItems: 'center',
  },
  title: {
    width: '30%',
  },
  textInputWrapper: {
    margin: 5,
    padding: 10,
    backgroundColor: '#1e1e1e',
    borderRadius: 20,
  },
  fakeButton: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'grey',
    borderRadius: 5,
  },
});
